**Explanation of method**

The method I used to solve this was to represent each move as an
axis-aligned bounding box. So for example, if the move of the robot
would be N4, W3, S3, E3 it would result in the following movements. 

![](N4W3S3E3.png)\

To be able to find overlapping squares, I represent each of these moves
as boxes with X1, Y1, X2, and Y2 coordinates representing the different
corners. So with the coordinate from the image, the movements would 
be translated as follows:


N4: X1(5) X2(5) Y1(5) Y2(1),

W3: X1(2) X2(5) Y1(5) Y2(5)

S3: X1(2) X2(2) Y1(5) Y2(2)

E3: X1(2) X2(5) Y1(2) Y2(2)


As the next step, In order to keep track of what part of a move is a
duplicate, allocate a byte array with the maximum area a robot can move
over in one command. The starting position is counted into this area. 
In the array, i use a "1" to represent a position that is overlapping.

**const int maxMoveLength = 100000;**
**byte[] lineOverlapping = new byte[maxMoveLength];**

The next step is to loop through each of these move boxes and compare
them with the boxes coming after them and check if they intersect. If
they do I will mark that position in the array with a 1. Then once I have 
looped through all upcoming move commands and stored a value for
each overlap, I will add the total area of the move to a Total variable and 
subtract the number of overlapping squares.

![](N4W3red.png)![](N4S3red.png)![](N4E3red.png)



 So taking the first line N4 I compare it to W3 first. The lines overlap
so I will write a 1 to the lineOverlapping array at offset 0 since this
is the top.           

 ![](Array0.png)

 The next check will be against line S3. N4 and S3 do not overlap so do
nothing. Compare N4 and E3, they do overlap so write a 1 at offset 3.

![](Array03.png) 

 Now we have checked the first move against all upcoming, count the
number of 1s in the array, and add them to a variable called
overlapping. Then add this value to a total count like this where
overlapping has the value 2. The total will now be 3. total +=
moveLength - overlapping; 

 **total: 3.** 
Now clear the array and loop again, starting with line W3 and
comparing it to the upcoming lines S3 and E3.

![](W3S3Red.png)![](W3E3Red.png)

 At the comparison with S3 we find that they collide at the leftmost
side so set the first byte in the array to 1.

 ![](Array0.png)



 Count the number of overlapping points and subtract it like the
previous example and the total should now be 3+3=6. 
 **Total: 6
**
Start the next step in the loop comparing S3 to E3. They intersect at point 3.

![](S3E3Red.png) 

 S3 and E 3 intersect at offset 3 so set this to 1. Count the number of
1s and subtract it from the move length. Total should now be 9.    

![](Array3.png) 

**total: 9**

In the last step there is no line in the list after E3 so just add the
full length of the line to the result and we should end up with a total
of 13.

 ![](E3Red.png)

 **total: 13**


**Earlier attempts**

One common thing people are curious about during an interview is the
thinking process when coming up to a solution so  I thought I would
write it down. 

The first thing that came to mind when I saw this assignment was to 
simply have a two-dimensional array grid for the whole field and set 
them to 1 when moving over to keep track of where the robot had 
been.   When testing this I quickly realized that keeping 200 000 x 
200 000 bytes in memory would require \> 40GB. Maybe a bit much 
for a  vacuum cleaner?

The next thing that came to mind was perhaps if I use bits instead of
 bytes I could save some space, but then realized that 40/8 is still a 
ridiculous  amount of memory, so no go.  

The next thought was that this data contains quite a bit of repetitive 
patterns, so maybe I can get it small enough if I compress it? I found  
support for compressing and decompressing whole arrays but no way 
to keep an array compressed and then have random read / write access. 
I felt that while it would be interesting to figure out how to create such
functionality it may be a bit overkill. I had some thoughts about dividing
the field into multiple parts and compress / decompress the relevant 
arrays as the robot moved onto them but this solution felt a bit clumsy so 
I  continued trying to find better ideas. 

The next step was to take a pen and paper and just draw some movement 
on paper. Doing this made me think of 2D bounding box collision detection 
and how I could use that to determine if lines overlap. If I determine if lines 
overlap I can find the intersections and would only have to allocate memory 
for one move at a time.  So that is how I came to choose the current 
implementation.
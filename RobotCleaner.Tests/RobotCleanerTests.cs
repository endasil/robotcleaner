using System;
using RobotCleaner.ConsoleApp;
using Xunit;

namespace RobotCleaner.Tests
{
    public class RobotTests
    {

        // Example scenario from the CINT PDF. This is what i started with when testing
        // the application.
        [Fact]
        public void ExampleFromPDF()
        {
            ConsoleApp.RobotCleaner robotCleaner = new ConsoleApp.RobotCleaner(
                10,
                22);
            robotCleaner.AddMoveInstruction(Direction.E, 2);
            robotCleaner.AddMoveInstruction(Direction.N, 1);
                Assert.Equal(4, RobotAnalyser.CalculateUniqueMoves(robotCleaner));
        }

        // After i got the example from the PDF to work i drew some arbitrary lines on paper
        // to have something more complex to use for verifying. 
        [Fact]
        public void Test1()
        {
            ConsoleApp.RobotCleaner robotCleaner = new ConsoleApp.RobotCleaner(
                10,
                10);
            robotCleaner.AddMoveInstruction(Direction.N, 5);
            robotCleaner.AddMoveInstruction(Direction.W, 3);
            robotCleaner.AddMoveInstruction(Direction.S, 2);
            robotCleaner.AddMoveInstruction(Direction.E, 5);
            robotCleaner.AddMoveInstruction(Direction.S, 2);
            robotCleaner.AddMoveInstruction(Direction.W, 4);
            robotCleaner.AddMoveInstruction(Direction.N, 6);
            Assert.Equal(24, RobotAnalyser.CalculateUniqueMoves(robotCleaner));
        }

        // This is the example from the readme file i wrote to explain my code.
        [Fact]
        public void ExampleFromReadmeHTML()
        {
            ConsoleApp.RobotCleaner robotCleaner = new ConsoleApp.RobotCleaner(
                5,
                1);
            robotCleaner.AddMoveInstruction(Direction.N, 4);
            robotCleaner.AddMoveInstruction(Direction.W, 3);
            robotCleaner.AddMoveInstruction(Direction.S, 3);
            robotCleaner.AddMoveInstruction(Direction.E, 3);

            Assert.Equal(13, RobotAnalyser.CalculateUniqueMoves(robotCleaner));
        }

        // Make sure that moving forward and backwards on the same path does not
        // affect the number of unique positions traversed, in this case East 
        // and West are tested.
        [Fact]
        public void MovingBackAndForthOnSamePathEWShouldNotIncreaseUniqueCount()
        {
            // Spawn a robot and place it on its start position
            ConsoleApp.RobotCleaner robotCleaner = new ConsoleApp.RobotCleaner(
             -100,
                -100);
            for (int i = 0; i < 100; i++)
            {
                robotCleaner.AddMoveInstruction(Direction.E, 999);
                robotCleaner.AddMoveInstruction(Direction.W, 999);
            }

            Assert.Equal(1000, RobotAnalyser.CalculateUniqueMoves(robotCleaner));
        }


        // Make sure that moving forward and backwards on the same path does not
        // affect the number of unique positions traversed, in this case North
        // and South are tested.
        [Fact]
        public void MovingBackAndForthOnSamePathNSShouldNotIncreaseUniqueCount()
        {
            // Spawn a robot and place it on its start position
            ConsoleApp.RobotCleaner robotCleaner = new ConsoleApp.RobotCleaner(
                -100,
                -100);
            for (int i = 0; i < 100; i++)
            {
                robotCleaner.AddMoveInstruction(Direction.N, 999);
                robotCleaner.AddMoveInstruction(Direction.S, 999);
            }

            Assert.Equal(1000, RobotAnalyser.CalculateUniqueMoves(robotCleaner));
        }


        // Test overlapping in all 4 directions
        [Fact]
        public void MovingBackAndForthOnPartOfSameTrackNSWEShouldIncreaseUniqueCount()
        {
            ConsoleApp.RobotCleaner robotCleaner = new ConsoleApp.RobotCleaner(
                0,
                0);
            for (int i = 0; i < 10; i++)
            {
                robotCleaner.AddMoveInstruction(Direction.N, 10);
                robotCleaner.AddMoveInstruction(Direction.E, 1);
                robotCleaner.AddMoveInstruction(Direction.S, 10);
            }
            for (int i = 0; i < 10; i++)
            {
                robotCleaner.AddMoveInstruction(Direction.W, 10);
                robotCleaner.AddMoveInstruction(Direction.N, 1);
                robotCleaner.AddMoveInstruction(Direction.E, 10);
            }

            Assert.Equal(121, RobotAnalyser.CalculateUniqueMoves(robotCleaner));
        }

    }
}

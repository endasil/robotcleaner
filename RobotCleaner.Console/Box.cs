﻿using System;

namespace RobotCleaner.ConsoleApp
{
    public class Box
    {
        public int X1 { get; }
        public int Y1 { get; }
        public int X2 { get; }
        public int Y2 { get; }
        public Box(int x1, int y1, int x2, int y2)
        {
            X1 = x1;
            Y1 = y1;
            X2 = x2;
            Y2 = y2;
        }

        public (int startOffset, int overlappingSquareCount) GetOverlapInfo(Box box2)
        {
            (Box intersectBox, int overlappingSquareCount) = this.GetOverlappingSquareAndCount(box2);

            // Offset is the distance between the left / top side of this new intersecting 
            // box and the other box. To determine if it is the x or y value that is to be
            // used, simply find the one with the highest value as the distance of the other
            // will be 0. 
            int startOffset = Math.Max(Math.Abs((int)(intersectBox.X1 - this.X1)), 
                Math.Abs((int)(intersectBox.Y1 - this.Y1)));
            return (startOffset, overlappingSquareCount);
        }

        public (Box, int) GetOverlappingSquareAndCount(Box box2)
        {

            // Leftmost point of the collision box is rightmost 
            // of the two x positions. Lower x means more left so 
            // highest x is rightmost.
            // X1 =3, Y1=4, X2= Y2=3
            //Y|   
            //5|   BBBBB
            //4| AAAAA B
            //3| A BBBBB
            //2| AAAAA
            //1|--------- 
            //X| 12345678
            //Y|   
            //5|   #####
            //4| ##CCC #
            //3| # CCC##
            //2| #####
            //1|--------- 
            //X| 12345678

            int intersectingX1 = Math.Max(this.X1, box2.X1);

            // Calculate the width by taking the leftmost of the
            // two right sides and subtracting the left side 
            // from it.
            int intersectingWidth = Math.Min(this.X2, box2.X2) - intersectingX1;
            int intersectingX2 = Math.Min(this.X2, box2.X2);

            // As higher Y means Y is further up. The lowest
            // of the two top Y1 values is the one closes 
            // to the bottom of the two tops and the one
            // that is the top of the intersecting square.
            int intersectingY1 = Math.Min(this.Y1, box2.Y1);

            int intersectingY2 = Math.Max(this.Y2, box2.Y2);
            int intersectingHeight = intersectingY1 - intersectingY2;
            var intersectingBox = new Box(intersectingX1, intersectingY1, intersectingX2, intersectingY2);
            return (intersectingBox, Math.Max(intersectingWidth, intersectingHeight) + 1);
        }

        public bool CollidesWith(Box box2)
        {
            // If the left side of the other box is to the right of this box right side, no collision
            //  1111 2222
            //  1  1 2  2
            //  1111 2222
            // If the right side of the other box is to the left of this box left side, no collision.
            //  2222 1111
            //  2  2 1  1
            //  2222 1111
            // If the top of the other box is below the bottom of this box, no collision 
            //  1111
            //  1  1
            //  1111
            //  2222
            //  2  2
            //  2222
            // If the bottom of the other box is above the top of this one, no collision
            //  2222
            //  2  2
            //  2222
            //  1111
            //  1  1
            //  1111

            if (box2.X1 > X2 || box2.X2 < X1 || box2.Y1 < Y2 || box2.Y2 > Y1)
                return false;
            
            return true;

        }
    }

}

﻿using System;

namespace RobotCleaner.ConsoleApp
{
    class Program
    {

        public static void Main()
        {
            
            //Console.WriteLine("How many different move commands will the robot get?\n");
            int nrOfLines = int.Parse(Console.ReadLine());

            //Console.WriteLine("Give starting x and y coordinates separated by space, expected to be between -100 000 and 100 000:\n");
            string[] startCoordinates = Console.ReadLine().Split(' ', 2);

            // Create a robot and set start position.
            RobotCleaner robotCleaner = new RobotCleaner(
                int.Parse(startCoordinates[0]),
                int.Parse(startCoordinates[1]));


            // Loop as many times as the number of commands and request input
            // for each, then add the command to the robots instruction stack.
            for (int i = 0; i < nrOfLines; i++)
            {
                //Console.WriteLine($"Give move command {i}: A direction N, S, W, E followed by a number representing the steps in that direction, ex N 10:\n");
                string[] moveData = Console.ReadLine().Split(' ');
                robotCleaner.AddMoveInstruction((Direction)Enum.Parse(typeof(Direction), moveData[0], true), int.Parse(moveData[1]));
            }

            // Run the analyser to figure out how many squres the program on the robot would cover.
            Console.WriteLine($"=> Cleaned: {RobotAnalyser.CalculateUniqueMoves(robotCleaner)}");
        }
    }
}

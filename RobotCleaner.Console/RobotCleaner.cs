﻿using System.Collections.Generic;

namespace RobotCleaner.ConsoleApp
{
    public class RobotCleaner
    {
        public List<MoveCommand> MoveCommands { get; }

        public int StartY { get; }
        public int StartX { get; }

        // Instantiate a robot and place it.
        public RobotCleaner(int startXPosition, int startYPosition)
        {
            StartX = startXPosition;
            StartY = startYPosition;
            MoveCommands = new List<MoveCommand>();
        }

        public void AddMoveInstruction(Direction direction, int distance)
        {
            MoveCommands.Add(new MoveCommand(direction, distance));
        }
    }
}

﻿namespace RobotCleaner.ConsoleApp
{
    public class MoveCommand
    {
        public Direction Direction { get; }
        public int Distance { get; }

        public MoveCommand(Direction direction, int distance)
        {
            Direction = direction;
            Distance = distance;
        }
    }
}

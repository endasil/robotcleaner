﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace RobotCleaner.ConsoleApp
{
    public class RobotAnalyser
    {
        public static int CalculateUniqueMoves(RobotCleaner robotCleaner)
        {
            // In order to figure out overlapping moves, each move command is 
            // interpetrated as an axis aligned bounding box.
            List<Box> boxes = Moves2BoundingBoxes(robotCleaner);
            int total = 0;
            
            // In order to keep track of what part of a move is a duplicate,
            // allocate a byte array with the maximum area a robot can move
            // over in one command. The starting position is counted into
            // this area. A 1 will be stored in each position where another
            // line crosses over this line and at the end will be used to
            // calculate how many unique squares this move occupied. 
            const int maxMoveLength = 100000;
            byte[] lineOverlapping = new byte[maxMoveLength];

            // Loop trough each move, then compare it to all moves that 
            // come after it to see if any of the other moves would cross it.
            for (int i = 0; i < boxes.Count; i++)
            {
                // Clean the part of the array that can hold information about lines crossing
                // over this move. Unsafe.InitBlock is about 10 times faster than Array.Fill.
                var moveLength = robotCleaner.MoveCommands[i].Distance + 1;
                Unsafe.InitBlock(ref lineOverlapping[0], 0, (uint)moveLength);
                
                // Go trough each move comming after this line to see if any of them move
                // over this movement line.
                    for (int j = i + 1; j < boxes.Count; j++)
                    {

                    // Do a simple box collision check to see if we need to do any deeper analysis
                    // of this move.
                    if (boxes[i].CollidesWith(boxes[j]))
                    {

                        // The moves overlap, get information about how many squares are occupied
                        // and at what part of the line it starts to overlap.
                        (int startOffset, int overlappingSquareCount) = boxes[i].GetOverlapInfo(boxes[j]);
                        
                        // Write a 1 for each overlapping block in overlapping line array.
                        Unsafe.InitBlock(ref lineOverlapping[startOffset], 1, (uint)overlappingSquareCount);
                    }
                }

                int overlapping = 0;

                // Count the number of overlapping position of this line. Doing it with
                // linq instead of for would be easier to read but worse performance.
                for (int pos = 0; pos < moveLength; pos++)
                {
                    if (lineOverlapping[pos] == 1)
                        overlapping++;
                }

                // Now add number of squares moved to the total count but, subtract the 
                // number of squares that were not unique for this line. These overlapping
                // squares will then be counted when the check is done for that other
                // overlapping move later.
                total += moveLength - overlapping;
            }

            return total;
        }

        private static List<Box> Moves2BoundingBoxes(RobotCleaner robotCleaner)
        {
            List<Box> boxes = new List<Box>();

            int xPosition = robotCleaner.StartX;
            int yPosition = robotCleaner.StartY;

            // Go trough each move and create a box representation for each of them.
            // In this coordinate system, positive Y is up (NORTH) and positive X is 
            // right (EAST). Y1 is the top (highest y value) and X1 is left (lowest
            // x value)
            foreach (var move in robotCleaner.MoveCommands)
            {
                switch (move.Direction)
                {
                    case Direction.N:
                        boxes.Add(new Box(xPosition, yPosition + move.Distance, xPosition, yPosition));
                        yPosition += move.Distance;
                        break;
                    case Direction.S:
                        boxes.Add(new Box(xPosition, yPosition, xPosition, yPosition - move.Distance));
                        yPosition -= move.Distance;
                        break;
                    case Direction.W:
                        boxes.Add(new Box(xPosition - move.Distance, yPosition, xPosition, yPosition));
                        xPosition -= move.Distance;
                        break;
                    case Direction.E:
                        boxes.Add(new Box(xPosition, yPosition, xPosition + move.Distance, yPosition));
                        xPosition += move.Distance;
                        break;
                }
            }

            return boxes;
        }
    }
}
